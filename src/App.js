import React from 'react';
import Main from './components/MainComponent';
import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import {configureStore} from './redux/configureStore'

const state = configureStore();

function App (props) {
    return (
    <Provider store={state}>
    	<BrowserRouter>
        	<div>
          		<Main />
        	</div>
      </BrowserRouter>
    </Provider>
    );
}

export default App;
