import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createForms } from 'react-redux-form';
import { Dishes } from './dishes';
import { Leaders } from './leaders';
import { Comments } from './comments';
import { Promotions } from './promotions';
import { initialFeedback } from './forms';

export const configureStore= ()=>{
	const store = createStore(
		combineReducers({
			dishes: Dishes,
			leaders: Leaders,
			comments: Comments,
			promotions: Promotions,
			...createForms ({
				feedback: initialFeedback

			})
		}),
		applyMiddleware(thunk, logger)

	)

	return store;
}