import * as ActionTypes from './ActionTypes';
import baseUrl from '../shares/baseUrl';

export const addComment = (comment) => ({
	type: ActionTypes.ADD_COMMENT,
	payload:comment

});

export const postComment = (dishId, rating, author, comment) => (dispatch) => {
    const newComment = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    };
    newComment.date = new Date().toISOString();

	return fetch(baseUrl+ 'comments',{
		method: "POST",
		body: JSON.stringify(newComment),
		headers:{
			"Content-Type": "application/json"
		},
		credentials: "same-origin"
	})
		.then(response=>{
			if(response.ok){
				return response;
			}
			else{
				let error = new Error(`Error ${response.status} : ${response.statusText}`);
				error.response = response;
				throw error;
			}
		},
		error => {
			throw error;
		})
		.then(response => response.json())
		.then(response => dispatch(addComment(response)))
		.catch(error => {console.log(error.message)})
}

export const fetchDishes = () => (dispatch)=>{
	dispatch(dishesLoading(true));
	return fetch(baseUrl+ 'dishes')
		.then(response=>{
			if(response.ok){
				return response;
			}
			else {
				let error = new Error(`Error  ${response.status}: ${response.statusText} `);
				error.response = response;
				throw error;
			}
		},
		error=>{
			let errormess = new Error(error.message);
			throw errormess;
		})
		.then(response=> response.json())
		.then (dishes => dispatch(addDishes(dishes)))
		.catch(error => dispatch(dishesFailed(error.message)))
}

export const dishesLoading = () => ({
	type: ActionTypes.DISHES_LOADING

});

export const dishesFailed = (errmsg) =>({
	type: ActionTypes.DISHES_FAILED,
	payload: errmsg

});

export const addDishes = (dishes) =>({
	type: ActionTypes.ADD_DISHES,
	payload: dishes

});

export const fetchPromos = ()=> (dispatch) => {
	dispatch(promosLoading(true));
	return fetch(baseUrl + 'promotions')
		.then(response=>{
			if(response.ok){
				return response;
			}
			else{
				let error = new Error(`Error  ${response.status}: ${response.statusText} `);
				error.response = response;
				throw error;
			}
		},
		error => {
			let errormess = new Error(error.message)
			throw errormess;
		})
		.then(response => response.json())
		.then(promotions => dispatch(addPromos(promotions)))
		.catch(error => dispatch(promosFailed(error.message)))
}

export const promosLoading = ()=> ({
	type: ActionTypes.PROMOS_LOADING
});

export const promosFailed =(errmsg) => ({
	type: ActionTypes.PROMOS_FAILED,
	payload: errmsg
});

export const addPromos = (promotions) => ({
	type: ActionTypes.ADD_PROMOS,
	payload: promotions
});

export const fetchComments =() => (dispatch) =>{
	return fetch(baseUrl + 'comments')
		.then(response =>{
			if(response.ok){
				return response;
			}
			else {
				let error = new Error(`Error ${response.status}: ${response.statusText}`);
				error.response = response;
				throw error;
			}
		},
		error => {
			let errormess = new Error(error.message);
			throw errormess;
		})
		.then(response => response.json())
		.then(comments => dispatch(addComments(comments)))
		.catch(error => dispatch(commentsFailed(error.message)))
}

export const commentsFailed = (errmsg) =>({
	type:ActionTypes.COMMENTS_FAILED,
	payload: errmsg
});

export const addComments = (comments) =>({
	type:ActionTypes.ADD_COMMENTS,
	payload: comments
});

export const fetchLeaders = () =>(dispatch)=>{
	dispatch(leadersLoading(true));
	return fetch(baseUrl+'leader')
	.then(response=>{
		if(response.ok){
			return response;
		}
		else {
			let error = new Error(`Error ${response.status} : ${response.statusText}`);
			error.response = response;
			throw error;
		}
	},
		error => {
			let errormess = new Error(error.message);
			throw errormess;
		}
	)
	.then(response => response.json())
	.then(leaders => dispatch(addLeaders(leaders)))
	.catch(error => dispatch(leadersFailed(error.message)))
}

export const leadersFailed = (errmsg) => ({
	type:ActionTypes.LEADERS_FAILED,
	payload: errmsg
})

export const leadersLoading = () => ({
	type:ActionTypes.LEADERS_LOADING
})

export const addLeaders = (leaders) => ({
	type:ActionTypes.ADD_LEADERS,
	payload: leaders
})

export const postFeedback = (feedback) =>(dispatch) =>{
	return fetch(baseUrl+ 'feedback',{
			method: 'POST',
			body: JSON.stringify(feedback),
			headers:{
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then(response => {
			if(response.ok){
				return response;
			}else{
				let error = new Error(`Error ${response.status} : ${response.statusText}`);
				error.response = response;
				throw error;
			}
		},
		error => {
			let errormess = new Error(error.message);
			throw errormess;
		})
		.then(response => response.json())
		.then(response => alert(JSON.stringify(response)))
		.catch(error => console.log(error.message))
}