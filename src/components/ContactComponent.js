import React from 'react';
import {Row, Col, Breadcrumb, BreadcrumbItem, Label, Button} from 'reactstrap';
import {Form, Control, Errors} from 'react-redux-form';
import {Link} from 'react-router-dom';


const required = (val)=> val && val.length;
const minLength = (len) => (val) => !(val) || (val.length >= len);
const maxLength = (len) => (val) => (val) && (val.length <= len);
const isNumber = (val) => !isNaN(Number(val));
const email = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);

class Contact extends React.Component{
	constructor(props){
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}



	handleSubmit(values){
		this.props.postFeedback(values);
		this.props.resetFeedbackForm();
	}

	render(){

		return(
			<div className="container">
				<Row>
					<Breadcrumb>
						<BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
						<BreadcrumbItem className="active">Contact Us</BreadcrumbItem>
					</Breadcrumb>
					<Col className="col-12">
						<h3>Contact Us</h3>
						<hr />
					</Col>
				</Row>
		    	<Row className="row-content">
		           <Col className="col-12">
		              <h3>Location Information</h3>
		           </Col>
		            <Col className="col-12 col-sm-4 offset-sm-1">
		                   <h5>Our Address</h5>
		                    <address>
				              121, Clear Water Bay Road<br />
				              Clear Water Bay, Kowloon<br />
				              HONG KONG<br />
				              <i className="fa fa-phone"></i>: +852 1234 5678<br />
				              <i className="fa fa-fax"></i>: +852 8765 4321<br />
				              <i className="fa fa-envelope"></i>:
		                        <a href="mailto:confusion@food.net">confusion@food.net</a>
				            </address>
		            </Col>
		            <Col className="col-12 col-sm-6 offset-sm-1">
		                <h5>Map of our Location</h5>
		            </Col>
		            <Col className="col-12 col-sm-11 offset-sm-1">
		                <div className="btn-group" role="group">
		                    <a className="btn btn-primary" href="tel:+13013025351"><i className="fa fa-phone"></i> Call</a>
		                    <a className="btn btn-info"><i className="fa fa-skype"></i> skype</a>
		                    <a className="btn btn-success" href="mailto:confusion@food.net"><i className="fa fa-envelope-o"></i> email</a>
		                </div>
		            </Col>
		        </Row>
		        <Row className="row-content">
		        	<Col className="col-12">
		        		<h3>Send us your Feedback</h3>
		        	</Col>
		        	<Col md={9}>
		        		<Form model="feedback" onSubmit={(values)=>this.handleSubmit(values)}>
		        			<Row className="form-group">
		        				<Label md={2} htmlFor="firstname">First Name</Label>
		        				<Col md={10}>
		        					<Control.text model=".firstname" name="firstname"
		        					id="firstname"
		        					placeholder="First Name"
		        					className="form-control"
		        					validators={{
		        						required,
		        						minLength: minLength(3),
		        						maxLength: maxLength(15)
		        					}}
		        					/>
		        					<Errors
		        						className="text-danger"
		        						model= ".firstname"
		        						show = "touched"
		        						messages = {{
			        						required: "Required ",
			        						minLength: "Must be greater than 2 Characters",
			        						maxLength: "Must be less than 16 Characters"
		        						}}

		        					/>

		        				</Col>
		        			</Row>
		        			<Row className="form-group">
		        				<Label md={2} htmlFor="lastname">Last Name</Label>
		        				<Col md={10}>
		        					<Control.text model=".lastname" name="lastname" id="lastname"
		        					placeholder="Last Name"
		        					className="form-control"
		        					validators={{
		        						required,
		        						minLength: minLength(3),
		        						maxLength: maxLength(15)
		        					}}
		        					/>
		        					<Errors
		        						className="text-danger"
		        						model= ".lastname"
		        						show = "touched"
		        						messages = {{
			        						required: "Required ",
			        						minLength: "Must be greater than 2 Characters",
			        						maxLength: "Must be less than 16 Characters"
		        						}}
		        					/>
		        				</Col>
		        			</Row>
		        			<Row className="form-group">
		        				<Label md={2} htmlFor="telnum">Contact Tel.</Label>
		        				<Col md={10}>
		        					<Control.text model=".telnum" name="telnum" id="telnum"
		        					placeholder="Tel number"
		        					className="form-control"
		        					validators={{
		        						required,
		        						isNumber,
		        						minLength: minLength(7),
		        						maxLength: maxLength(13)
		        					}}
		        					/>
		        					<Errors
		        						className="text-danger"
		        						model= ".telnum"
		        						show = "touched"
		        						messages = {{
			        						required: "Required ",
		        							isNumber: "Must be a number",
			        						minLength: "Must be greater than 6 Characters",
			        						maxLength: "Must be less than 14 Characters"
		        						}}
		        					/>
		        				</Col>
		        			</Row>
		        			<Row className="form-group">
		        				<Label md={2} htmlFor="email">Email</Label>
		        				<Col md={10}>
		        					<Control.text model=".email" name="email" id="email"
		        					placeholder="Email"
		        					className="form-control"validators={{
		        						required,
		        						email
		        					}}
		        					/>
		        					<Errors
		        						className="text-danger"
		        						model= ".email"
		        						show = "touched"
		        						messages = {{
			        						required: "Required ",
		        							email: "Email is not valid"
		        						}}
		        					/>
		        				</Col>
		        			</Row>
		        			<Row className="form-group">
		        				<Col md={{size:6, offset:2}}>
		        					<div className="form-check">
		        						<Label check>
			        						<Control.checkbox model=".agree" name="agree"
			        						className="form-check-input"/> {' '}
			        						<strong>May we contact you?</strong>
		        						</Label>
		        					</div>
		        				</Col>
		        				<Col md={{size:3, offset:1}}>
		        					<Control.select model=".contactType" name="contactType"
		        					className="form-control">
		        						<option>Tel</option>
		        						<option>Email</option>
		        					</Control.select>
		        				</Col>
		        			</Row>
		        			<Row className="form-group">
		        				<Label md={2} htmlFor="message">Your Feedback</Label>
		        				<Col md={10}>
		        					<Control.textarea model=".message" name="message" id="message"
		        					rows="12"
		        					className="form-control"
		        					/>
		        				</Col>
		        			</Row>
		        			<Row className="form-group">
		        				<Col md={{size:10, offset:2}}>
		        					<Button color="primary">Send Feedback</Button>
		        				</Col>
		        			</Row>
		        		</Form>
		        	</Col>
		        	<Col md={3}></Col>
		        </Row>
		    </div>

		)
	}

}

export default Contact;