import React  from 'react';

export const Loading = ()=>{
	return(
		<div className="col-12">
			<span className="fa fa-spinner fa-pause fa-3x fa-fw text-primary"></span>
			<h4>Loading .... </h4>
		</div>
	)
}