import React from 'react';
import {Card, CardImg, CardBody, CardTitle, CardText, Row, Col, Breadcrumb, BreadcrumbItem,
Button, Label, Modal, ModalHeader, ModalBody} from 'reactstrap';
import {Link} from 'react-router-dom';
import {LocalForm, Control, Errors} from 'react-redux-form';
import { Loading } from './LoadingComponent';
import baseUrl from '../shares/baseUrl';
import {FadeTransform, Stagger, Fade} from 'react-animation-components';

const required = (val)=> val && val.length;
const maxLength = (len)=> (val)=> !(val) || (val.length <= len);
const minLength = (len)=> (val)=> (val) && (val.length >= len);

class CommentForm  extends React.Component{
	constructor(props){
		super(props);
		this.state={
			isCommentModalOpen: false
		}
		this.openCommentModal = this.openCommentModal.bind(this);
		this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
	}

	openCommentModal(){
		this.setState({
			isCommentModalOpen:!this.state.isCommentModalOpen
		})
	}

	handleCommentSubmit(values){
		this.openCommentModal();
		this.props.postComment(this.props.dishId, values.rating, values.author, values.comment)
	}

	render(){
		return(
			<>
				<Button type="submit" className="btn btn-outline-secondary btn-light"
				onClick = {this.openCommentModal}>
					<span className="fa fa-lg fa-pencil"> </span>
					&nbsp;Submit Comment
				</Button>
				<Modal isOpen={this.state.isCommentModalOpen} toggle={this.openCommentModal}>
					<ModalHeader toggle={this.openCommentModal}>
						<h3>Submit Comment</h3>
					</ModalHeader>
					<ModalBody>
						<LocalForm onSubmit={(values)=> this.handleCommentSubmit(values)}>
							<Row className="form-group">
								<Label htmlFor="rating" sm={12}>Rating</Label>
								<Col sm={12}>
									<Control.select model=".rating" name="rating"
									id="rating" className="form-control">
										<option selected>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</Control.select>
								</Col>
							</Row>
							<Row className="form-group">
								<Label htmlFor="name" sm={12}>Your Name</Label>
								<Col sm={12}>
									<Control.text model=".author" name="author"
									id="author" className="form-control"
									placeholder="Your Name"
									validators={{
										required,
										minLength: minLength(3),
										maxLength: maxLength(15)
									}}/>
									<Errors
										className="text-danger"
										model=".author"
										show="touched"
										messages={{
											required: "Required ",
											minLength: "Must be greater than 2 characters ",
											maxLength: "Must be 15 characters or less"
										}}
									/>
								</Col>
							</Row>
							<Row className="form-group">
								<Label htmlFor="comments" sm={12}>Comments</Label>
								<Col sm={12}>
									<Control.textarea model=".comments" name="comments"
									id="comments" className="form-control"
									rows="6" />
								</Col>
							</Row>
							<Row className="form-group">
		        				<Col sm={12}>
		        					<Button color="primary">Submit</Button>
		        				</Col>
		        			</Row>
						</LocalForm>
					</ModalBody>
				</Modal>
			</>
		)
	}
}



function RenderDish ({dish}) {
	return(
		<Col className= "col-12 col-md-5 m-1">
			<FadeTransform in
				transformProps={{
					exitTransform:'scale(0.5) translateY(-50%)'
			}}>
				<Card>
					<CardImg width="100%" src={baseUrl+dish.image} alt={dish.name} />
					<CardBody>
						<CardTitle>{dish.name}</CardTitle>
						<CardText>{dish.description}</CardText>
					</CardBody>
				</Card>
			</FadeTransform>
		</Col>
	)
}

function RenderComments({comments, dishId, postComment, commentsErrorMsg}){
	if(commentsErrorMsg){
		return(
			<Col className="col-12 col-md m-1">
				<h4>{commentsErrorMsg}</h4>
			</Col>
		)
	}
	else {
		return(
			<Col className= "col-12 col-md-5 m-1">
				<h3>Comments</h3>
				<ul className="list-unstyled">
					<Stagger in>
						{comments.map((comment) => {
							return(
								<Fade in>
									<li key={comment.id}>
										<p>{comment.comment}</p>
										<p>
											--{comment.author},
											{new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}
										</p>
									</li>
								</Fade>
							)
						})}
					</Stagger>
				</ul>
				<CommentForm dishId={dishId} postComment={postComment} />
			</Col>
		)
	}
}

const DishDetail = (props) => {
	if(props.isLoading){
		return(
			<Loading />
		);
	}
	else if(props.errmsg){
		return(
			<h4>{props.errmsg}</h4>
		);
	}
	else if(props.dish != null){
		return(
			<div className="container">
				<Row>
					<Breadcrumb>
						<BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
						<BreadcrumbItem className="active">{props.dish.name}</BreadcrumbItem>
					</Breadcrumb>
					<Col className="col-12">
						<h3>{props.dish.name}</h3>
						<hr />
					</Col>
				</Row>
				<Row>
					<RenderDish dish = {props.dish} />
					<RenderComments comments={props.comments}
					postComment={props.postComment}
					dishId={props.dish.id}
					commentsErrorMsg={props.commentsErrorMsg}
					/>
				</Row>
			</div>
		)
	}else
	{
		return (<div></div>);
	}
}


export default DishDetail;