import React  from 'react';
import { Card, CardImg, CardImgOverlay, CardTitle, Row, Col, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import {Link} from 'react-router-dom';
import { Loading } from './LoadingComponent';
import baseUrl from '../shares/baseUrl';

function RenderMenu ({dish}){
	return(
		<Card>
			<Link to = {`/menu/${dish.id}`} >
				<CardImg width="100%" src={baseUrl+dish.image} alt={dish.name} />
				<CardImgOverlay>
					<CardTitle>{dish.name}</CardTitle>
				</CardImgOverlay>
			</Link>
		</Card>
	)
}

const Menu =(props) => {
	const menu = props.dishes.dishes.map((dish) => {
		return(
			<Col key={dish.id} className="col-12 col-md-5 m-1">
				<RenderMenu dish = {dish} />
			</Col>
		)
	});
	if(props.dishes.isLoading){
		return(
			<Loading />
		);
	}
	else if(props.dishes.errmsg){
		return(
			<h4>{props.dishes.errmsg}</h4>
		);
	}
	else
		return(
			<div className="container">
				<Row>
					<Breadcrumb>
						<BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
						<BreadcrumbItem className="active">Menu</BreadcrumbItem>
					</Breadcrumb>
					<Col className="col-12">
						<h3>Menu</h3>
						<hr />
					</Col>
				</Row>
				<Row className="row-content">
					{menu}
					<Col className="col-md"></Col>
				</Row>
			</div>
		);
}


export default Menu;
