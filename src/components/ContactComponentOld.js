import React from 'react';
import {Row, Col, Breadcrumb, BreadcrumbItem, Form, FormGroup, Label, Button, Input, FormFeedback} from 'reactstrap';
import {Link} from 'react-router-dom';

class ContactOld extends React.Component{
	constructor(props){
		super(props);
		this.state={
			firstname: '',
			lastname: '',
			telnum: '',
			email: '',
			agree: true,
			contactType: 'Email',
			message:'',
			touched:{
				firstname: false,
				lastname: false,
				telnum: false,
				email: false
			}
		}
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleInputBlur = this.handleInputBlur.bind(this);
	}

	handleInputChange(event){
		const target = event.target;
		const name = target.name;
		const value = target.type === "checkbox" ? target.checked : target.value;

		this.setState({
			[name]:value
		})

	}

	handleInputBlur=(field)=>(event)=>{
		this.setState({
			touched: {...this.state.touched, [field]: true}
		})
	}

	handleSubmit(event){
		alert(JSON.stringify(this.state));
		event.preventDefault();
	}

	validateForm(firstname,lastname,telnum,email){
		const errors = {
			firstname: '',
			lastname: '',
			telnum: '',
			email: '',
		}

		if(this.state.touched.firstname && (firstname.length<3 || firstname.length>10))
			errors.firstname = "First Name should be min 3 Character and Maximum 10 Character"

		if(this.state.touched.lastname && (lastname.length<3 || lastname.length>16))
			errors.lastname = "Last Name should be min 3 Character and Maximum 16 Character"

		const regtel = /^\d+$/;
		if(this.state.touched.telnum && !regtel.test(telnum))
			errors.telnum = "Telephone number will contain number only"
		else if(this.state.touched.telnum && telnum.length!==10 )
			errors.telnum = "Telephone number must contains 10 number"
		if(this.state.touched.email && email.split('').filter(x=> x==='@').length!==1)
			errors.email = "Email address must contain @"

		return errors;
	}

	render(){
		const errors = this.validateForm(this.state.firstname,this.state.lastname,this.state.telnum,this.state.email);
		return(
			<div className="container">
				<Row>
					<Breadcrumb>
						<BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
						<BreadcrumbItem className="active">Contact Us</BreadcrumbItem>
					</Breadcrumb>
					<Col className="col-12">
						<h3>Contact Us</h3>
						<hr />
					</Col>
				</Row>
		    	<Row className="row-content">
		           <Col className="col-12">
		              <h3>Location Information</h3>
		           </Col>
		            <Col className="col-12 col-sm-4 offset-sm-1">
		                   <h5>Our Address</h5>
		                    <address>
				              121, Clear Water Bay Road<br />
				              Clear Water Bay, Kowloon<br />
				              HONG KONG<br />
				              <i className="fa fa-phone"></i>: +852 1234 5678<br />
				              <i className="fa fa-fax"></i>: +852 8765 4321<br />
				              <i className="fa fa-envelope"></i>:
		                        <a href="mailto:confusion@food.net">confusion@food.net</a>
				            </address>
		            </Col>
		            <Col className="col-12 col-sm-6 offset-sm-1">
		                <h5>Map of our Location</h5>
		            </Col>
		            <Col className="col-12 col-sm-11 offset-sm-1">
		                <div className="btn-group" role="group">
		                    <a className="btn btn-primary" href="tel:+13013025351"><i className="fa fa-phone"></i> Call</a>
		                    <a className="btn btn-info"><i className="fa fa-skype"></i> skype</a>
		                    <a className="btn btn-success" href="mailto:confusion@food.net"><i className="fa fa-envelope-o"></i> email</a>
		                </div>
		            </Col>
		        </Row>
		        <Row className="row-content">
		        	<Col className="col-12">
		        		<h3>Send us your Feedback</h3>
		        	</Col>
		        	<Col md={9}>
		        		<Form onSubmit={this.handleSubmit}>
		        			<FormGroup row>
		        				<Label md={2} htmlFor="firstname">First Name</Label>
		        				<Col md={10}>
		        					<Input type="text" name="firstname" id="firstname"
		        					placeholder="First Name" value={this.state.firstname}
		        					valid= {errors.firstname === ''}
		        					invalid= {errors.firstname !== ''}
		        					onBlur = {this.handleInputBlur('firstname')}
		        					onChange={this.handleInputChange}/>
		        					<FormFeedback>{errors.firstname}</FormFeedback>
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Label md={2} htmlFor="lastname">Last Name</Label>
		        				<Col md={10}>
		        					<Input type="text" name="lastname" id="lastname"
		        					placeholder="Last Name" value={this.state.lastname}
		        					valid= {errors.lastname === ''}
		        					invalid= {errors.lastname !== ''}
		        					onBlur = {this.handleInputBlur('lastname')}
		        					onChange={this.handleInputChange} />
		        					<FormFeedback>{errors.lastname}</FormFeedback>
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Label md={2} htmlFor="telnum">Contact Tel.</Label>
		        				<Col md={10}>
		        					<Input type="tel" name="telnum" id="telnum"
		        					placeholder="Tel number" value={this.state.telnum}
		        					valid= {errors.telnum === ''}
		        					invalid= {errors.telnum !== ''}
		        					onBlur = {this.handleInputBlur('telnum')}
		        					onChange={this.handleInputChange} />
		        					<FormFeedback>{errors.telnum}</FormFeedback>
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Label md={2} htmlFor="email">Email</Label>
		        				<Col md={10}>
		        					<Input type="email" name="email" id="email"
		        					placeholder="Email" value={this.state.email}
		        					valid= {errors.email === ''}
		        					invalid= {errors.email !== ''}
		        					onBlur = {this.handleInputBlur('email')}
		        					onChange={this.handleInputChange} />
		        					<FormFeedback>{errors.email}</FormFeedback>
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Col md={{size:6, offset:2}}>
		        					<FormGroup check>
		        						<Input type="checkbox" name="agree"
		        						checked={this.state.agree} onChange={this.handleInputChange}/> {' '}
		        						<strong>May we contact you?</strong>
		        					</FormGroup>
		        				</Col>
		        				<Col md={{size:3, offset:1}}>
		        					<Input type="select" name="contactType"
		        					value={this.state.contactType}
		        					onChange={this.handleInputChange}>
		        						<option>Tel</option>
		        						<option>Email</option>
		        					</Input>
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Label md={2} htmlFor="firstname">Your Feedback</Label>
		        				<Col md={10}>
		        					<Input type="textarea" name="message" id="message"
		        					row="12" value={this.state.message}
		        					onChange={this.handleInputChange} />
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Col md={{size:10, offset:2}}>
		        					<Button color="primary">Send Feedback</Button>
		        				</Col>
		        			</FormGroup>
		        		</Form>
		        	</Col>
		        	<Col md={3}></Col>
		        </Row>
		    </div>

		)
	}

}

export default ContactOld;