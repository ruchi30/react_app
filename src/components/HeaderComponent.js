import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavbarToggler, Collapse, Jumbotron,
Button, Modal, ModalHeader, ModalBody, Row, Col, Form, FormGroup, Input, Label } from 'reactstrap';
import {NavLink} from 'react-router-dom';

class Header extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			isNavOpen : false,
			isModalOpen: false
		};
		this.toggleNav = this.toggleNav.bind(this);
		this.toggleModal = this.toggleModal.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
	}

	toggleNav() {
		this.setState({
			isNavOpen : !this.state.isNavOpen
		})
	}

	toggleModal(){
		this.setState({
			isModalOpen: !this.state.isModalOpen
		})
	}

	handleLogin(event){
		this.toggleModal();
		alert(`Username : ${this.username.value}, Password : ${this.password.value}, Remember: ${this.remember.checked}`);
		 event.preventDefault();
	}

	render(){
		return(
			<>
				<Navbar dark expand="md">
		          <div className="container">
		          	<NavbarToggler onClick={this.toggleNav}/>
		            <NavbarBrand className="mr-auto" href="./">
		            	<img src="assets/images/logo.png" height="30" width="41" alt="Ristorante Con Fusion" />
		            </NavbarBrand>
		            <Collapse isOpen = {this.state.isNavOpen} navbar>
		            	<Nav navbar>
		            		<NavItem>
		            			<NavLink className="nav-link" to="/home">
		            				<span className="fa fa-home fa-lg"></span> Home
		            			</NavLink>
		            		</NavItem>
		            		<NavItem>
		            			<NavLink className="nav-link" to="/aboutus">
		            				<span className="fa fa-info fa-lg"></span> About Us
		            			</NavLink>
		            		</NavItem>
		            		<NavItem>
		            			<NavLink className="nav-link" to="/menu">
		            				<span className="fa fa-list fa-lg"></span> Menu
		            			</NavLink>
		            		</NavItem>
		            		<NavItem>
		            			<NavLink className="nav-link" to="/contactus">
		            				<span className="fa fa-address-card fa-lg"></span> Contact Us
		            			</NavLink>
		            		</NavItem>
		            	</Nav>
		            	<Nav navbar className="ml-auto">
		            		<NavItem>
		            			<Button color="primary" onClick={this.toggleModal}>
		            				<span className="fa fa-lg fa-user"></span> Login
		            			</Button>
		            		</NavItem>
		            	</Nav>
		            </Collapse>
		          </div>
		        </Navbar>
		        <Jumbotron>
					<div className="container">
			            <Row className="row-header">
			                <Col className="col-12 col-sm-6">
			                    <h1>Ristorante con Fusion</h1>
			                    <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
			                </Col>
			                <Col className="col-12 col-sm"></Col>
			            </Row>
			        </div>
		        </Jumbotron>
		        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
		        	<ModalHeader toggle={this.toggleModal}>
		        		<h3>Log in</h3>
		        	</ModalHeader>
		        	<ModalBody>
		        		<Form onSubmit={this.handleLogin}>
		        			<FormGroup row>
		        				<Label md={3} htmlFor="username">Username</Label>
		        				<Col md={9}>
		        					<Input type="text" name="username" id="username"
		        					placeholder="Username"
		        					innerRef={(input)=> this.username = input} />
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Label md={3} htmlFor="password">Password</Label>
		        				<Col md={9}>
		        					<Input type="password" name="password" id="password"
		        					placeholder="Password"
		        					innerRef={(input)=> this.password = input} />
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Col md={{size:9, offset:3}}>
		        					<FormGroup check>
		        						<Label check>
		        							<Input type="checkbox" name="remember" id="remember"
		        							innerRef={(input)=> this.remember = input} />
		        							Remember Me
		        						</Label>
		        					</FormGroup>
		        				</Col>
		        			</FormGroup>
		        			<FormGroup row>
		        				<Col md={{size:9, offset:3}}>
		        					<Button type="submit" color="primary" value="submit">Signin</Button>
		        				</Col>
		        			</FormGroup>
		        		</Form>
		        	</ModalBody>
		        </Modal>
	        </>
		)
	}
}

export default Header;