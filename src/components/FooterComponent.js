import React from 'react';
import {Row, Col } from 'reactstrap';
import {Link} from 'react-router-dom';

function Footer(props){
	return(
		<footer className="footer">
	        <div className="container">
	            <Row>
	                <Col className="col-4 col-sm-2 offset-1">
	                    <h5>Links</h5>
	                    <ul className="list-unstyled">
	                        <li><Link to="/home">Home</Link></li>
	                        <li><Link to="/aboutus">About</Link></li>
	                        <li><Link to="/menu">Menu</Link></li>
	                        <li><Link to="/contactus">Contact</Link></li>
	                    </ul>
	                </Col>
	                <Col className="col-7 col-sm-5">
	                    <h5>Our Address</h5>
	                    <address>
			              121, Clear Water Bay Road<br />
			              Clear Water Bay, Kowloon<br />
			              HONG KONG<br />
			              <i className="fa fa-phone fa-lg"></i>: +852 1234 5678<br />
			              <i className="fa fa-fax fa-lg"></i>: +852 8765 4321<br />
			              <i className="fa fa-envelope fa-lg"></i>: <a href="mailto:confusion@food.net">confusion@food.net</a>
			           </address>
	                </Col>
	                <Col className="col-12 col-sm-4  align-self-center">
	                    <div className="text-center">
	                        <a href="http://google.com/+" className="btn btn-social-icon btn-google"><i className="fa fa-google-plus"></i></a>
	                        <a href="http://www.facebook.com/profile.php?id=" className="btn btn-social-icon btn-facebook"><i className="fa fa-facebook"></i></a>
	                        <a href="http://www.linkedin.com/in/" className="btn btn-social-icon btn-linkedin"><i className="fa fa-linkedin"></i></a>
	                        <a href="http://twitter.com/" className="btn btn-social-icon btn-twitter"><i className="fa fa-twitter"></i></a>
	                        <a href="http://youtube.com/" className="btn btn-social-icon btn-google"><i className="fa fa-youtube"></i></a>
	                        <a href="mailto:confusion@food.net" className="btn btn-social-icon btn-linkedin"><i className="fa fa-envelope-o"></i></a>
	                    </div>
	                </Col>
	           </Row>
	           <Row className="justify-content-center">
	                <Col className="col-auto justify-content-center">
	                    <p>© Copyright 2018 Ristorante Con Fusion</p>
	                </Col>
	           </Row>
	        </div>
	    </footer>
	)
}

export default Footer;