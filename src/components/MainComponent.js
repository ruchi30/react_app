import React from 'react';
import { actions } from 'react-redux-form';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import DishDetail from './DishdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {postComment, fetchDishes, fetchPromos, fetchComments, fetchLeaders, postFeedback} from '../redux/ActionCreators';
import {TransitionGroup, CSSTransition} from 'react-transition-group';

const mapStateToProps= (state)=>{
  return{
    dishes: state.dishes,
    promotions: state.promotions,
    leaders: state.leaders,
    comments: state.comments
  }

}

const dispatchStateToProps= (dispatch) =>({
  postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
  fetchDishes: () => {dispatch(fetchDishes())},
  fetchPromos: () => {dispatch(fetchPromos())},
  fetchComments: () => {dispatch(fetchComments())},
  fetchLeaders: () => {dispatch(fetchLeaders())},
  resetFeedbackForm: () => {dispatch(actions.reset('feedback'))},
  postFeedback: (feedback)=> {dispatch(postFeedback(feedback))}
});

class Main extends React.Component {

  componentDidMount(){
    this.props.fetchDishes();
    this.props.fetchPromos();
    this.props.fetchComments();
    this.props.fetchLeaders();
  }

  render(){
    const HomePage = () =>{
      return(
        <Home dish = {this.props.dishes.dishes.filter((dish)=>dish.featured)[0]}
          dishesLoading= {this.props.dishes.isLoading}
          dishesErrMsg= {this.props.dishes.errmsg}
          promotion = {this.props.promotions.promotions.filter((promotion) => promotion.featured)[0]}
          promosLoading = {this.props.promotions.isLoading}
          promosErrMsg= {this.props.promotions.errmsg}
          leader = {this.props.leaders.leaders.filter((leader)=> leader.featured)[0]}
          leaderLoading= {this.props.leaders.isLoading}
          leaderErrMsg= {this.props.leaders.errmsg}
        />
      );
    }

    const DishWithId = ({match}) =>{
      return(
        <DishDetail dish = {this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
          isLoading= {this.props.dishes.isLoading}
          errmsg= {this.props.dishes.errmsg}
          comments = {this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10))}
          commentsErrorMsg ={this.props.comments.errmsg}
          postComment={this.props.postComment}
        />
      );
    }

    return (
      <div>
        <Header />
        <TransitionGroup>
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
            <Switch location={this.props.location}>
              <Route path="/home" component = {HomePage} />
              <Route exact path="/menu" component={()=> <Menu dishes={this.props.dishes} /> } />
              <Route path="/menu/:dishId" component={DishWithId} />
              <Route exact path="/aboutus" component = {()=> <About
                leaders={this.props.leaders.leaders}
                isloading= {this.props.leaders.isLoading}
                errmsg= {this.props.leaders.errmsg} />}
              />
              <Route exact path="/contactus" component = {() => <Contact
                resetFeedbackForm={this.props.resetFeedbackForm}
                postFeedback = {this.props.postFeedback} />} />
              <Redirect to="/home" />
            </Switch>
          </CSSTransition>
        </TransitionGroup>
        <Footer />
      </div>
    );
  }

}

export default withRouter((connect)(mapStateToProps, dispatchStateToProps)(Main));
