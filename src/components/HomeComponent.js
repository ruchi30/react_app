import React from 'react';
import {Row, Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText} from 'reactstrap';
import { Loading } from './LoadingComponent';
import baseUrl from '../shares/baseUrl';
import {FadeTransform} from 'react-animation-components';

function RenderCard({item, isloading, errmsg}){
	if(isloading){
		return(
			<Col className="col-12 col-md m-1">
				<Loading />
			</Col>
		);
	}
	else if(errmsg){
		return(
			<Col className="col-12 col-md m-1">
				<h4>{errmsg}</h4>
			</Col>
		);
	}
	else
		return(
			<Col className="col-12 col-md m-1">
			<FadeTransform in
				transformProps={{
					exitTransform:'scale(0.5) translateY(-50%)'
				}}>
					<Card>
						<CardImg src={baseUrl + item.image} alt={item.name} />
						<CardBody>
							<CardTitle>{item.name}</CardTitle>
							{item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null}
							<CardText>{item.description}</CardText>
						</CardBody>
					</Card>
				</FadeTransform>
			</Col>
		)
}

function Home(props){
	return(
		<div className="container">
			<Row className="align-items-start">
				<RenderCard item={props.dish} isloading={props.dishesLoading} errmsg={props.dishesErrMsg} />
				<RenderCard item={props.promotion} isloading={props.promosLoading} errmsg={props.promosErrMsg}  />
				<RenderCard item={props.leader} isloading ={props.leaderLoading} errmsg={props.leaderErrMsg} />
			</Row>
		</div>
	);
}

export default Home;